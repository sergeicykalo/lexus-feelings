$(function() {
    if(jQuery.browser.mobile)
        init_preload([
            'assets/img/mobile/bg.jpg',
            'assets/img/mobile/bg-landscape.jpg',
            'assets/img/mobile/info-bg-1.png',
            'assets/img/mobile/info-bg-2.png',
            'assets/img/mobile/info-bg-3.png',
            'assets/img/mobile/info-bg-4.png',
            'assets/img/mobile/info-bg-5.png',
            'assets/img/mobile/play_youtube.png',
            'assets/img/mobile/logo.png'])
    else
        init_preload([
            'assets/img/info/info-1.png',
            'assets/img/info/info-2.png',
            'assets/img/info/info-3.png',
            'assets/img/info/info-4.png',
            'assets/img/info/info-5.png',
            'assets/img/men/men-1.png',
            'assets/img/men/men-1-ipad.png',
            'assets/img/men/men-2.png',
            'assets/img/leaves.png',
            'assets/img/leaves-top.png',
            'assets/img/leaves_big.png',
            'assets/img/logo.png',
            'assets/img/menu-marker.png',
            'assets/img/start-info.png',
            'assets/img/steps.png',
            'assets/img/bg/1.jpg',
            'assets/img/bg/1-1.jpg',
            'assets/img/bg/2.jpg',
            'assets/img/bg/3.jpg',
            'assets/img/bg/4.jpg',
            'assets/video/poster/1.jpg',
            'assets/video/poster/2.jpg',
            'assets/video/poster/3.jpg',
            'assets/video/poster/4.jpg',
            'assets/video/poster/5.jpg',
            'assets/video/poster/6.jpg',
            'assets/video/poster/7.jpg'])

});


var imgPreloadArr = [];
var totalNumImgPreload = -1;
var numImgPreload = 0;

function init_preload(var_imgPreloadArr)
{
    imgPreloadArr = var_imgPreloadArr;

    $("#preloader").fadeIn(800, function() { preloadImg() });
}

function preloadImg()
{
    if (totalNumImgPreload == -1)
    {
        var htmlImg = '';
        for (imgItem in imgPreloadArr)
        {
            htmlImg = htmlImg + '<img src="'+imgPreloadArr[imgItem]+'" alt="" onload="preloadImg()" />';
            totalNumImgPreload++;
        }
        $("#preload_img").html(htmlImg);
    }
    else
    {
        if (totalNumImgPreload == numImgPreload)
        {
            $("img.logo").show()
            TweenLite.to($("div#top_menu"),1 , {'top':0, ease:'Strong.easeOut'} );
            $('html, body').animate({scrollTop: 0 }, {duration: 100})
            $("#preloader").fadeOut(800, function() {
                init_main();
            });
        }
        else
        {
            numImgPreload++;
            $("#preloader div").css("width", (308 * (numImgPreload / totalNumImgPreload)) + "px");
        }
    }
}

function init_main(){
    setTimeout(function(){
        if( window.location.hash.indexOf('video') > -1 && getURLParameter('s') == 'qr' ){
            checkInAnalytic('qr_video')
            if(jQuery.browser.mobile)
                scrollToMobile('youtube_mobile')
            else
                scrollTo('step_6')
        }
    }, 800)


    if(jQuery.browser.mobile){
        $("div#mobile").fadeIn()
    }else
        $("div#content").css({"opacity": 0, "visibility":"visible" }).animate({"opacity": 1}, 800);

//    if (typeof($('#song').get(0).play)=='function') $('#song').get(0).play();
    $("p.play").trigger('click')
    $("div.on").fadeIn()
}