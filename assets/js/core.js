var Goto = { 'step_1':'div.info-1',
    'step_2':'div.info-2',
    'step_2_2':'div.info-2-2',
    'step_3':'div.info-3',
    'step_4':'div.info-4',
    'step_5':'div.slide-4',
    'step_6':'div.info-5',
    'youtube_mobile':'div#mobile-info-6',
    'step_start':0}

var delta = 0,
    scrolled = 0,
    video_paused = true,
    is_ipad = false,
    startpos = 0,
    curscroll = 0,
    diffscroll = 0,
    endscroll = 0;

$(document).ready(function(){

    if (navigator.userAgent.match(/iPad/i) != null)
        is_ipad = true;

    if( is_ipad ){
        checkInAnalytic('desktop')
        ipadScrollInit()
    }else if(jQuery.browser.mobile){
        checkInAnalytic('mobile')
        mobileInit()
    }else{
        checkInAnalytic('desktop')
        $(window).bind('scroll',function(e){
            parallaxScroll();
            delta =  $(document).height() - $(window).height()
        });
    }
})

function parallaxScroll(){

    videoPlayChecker()

    scrolled = $(window).scrollTop();

    $('div.slide-1').css('top',(0-(scrolled*.20))+'px');
    $('div.slide-2').css('top',(2743-(scrolled*.25))+'px');
    $('div.slide-3, div.slide-3-1').css('top',(4033-(scrolled*.31))+'px');
    $('div.slide-4, div.slide-4-1, div.slide-4-2').css('top',(5766-(scrolled*.32))+'px');
    $('div.leaves').css('top',(2250-(scrolled*.5))+'px');

    $('div.leaves-top').css('top',(3670-(scrolled*.7))+'px');

    $('div.info-start').css('top',(140-(scrolled*.7))+'px');
    $('div.info-1').css('top',(970-(scrolled*.5))+'px');
    $('div.info-2').css('top',(1590-(scrolled*.5))+'px');
    $('div.info-2-2').css('top',(3600-(scrolled*.8))+'px');
    $('div.info-3').css('top',(4170-(scrolled*.5))+'px');
    $('div.info-4').css('top',(6000-(scrolled*.5))+'px');
    $('div.info-5').css('top',(8000-(scrolled*.51))+'px');

    $('div.men-1').css('top',(540-(scrolled*.8))+'px');
    $('div.men-2').css('top',(3590-(scrolled*.6))+'px');

    parallaxFadeIn(scrolled)
}

function parallaxFadeIn(scrolled){
//    elemFadeInOut($('div.info-5'), scrolled, 900, -100)

    if( !arrow_down_click ){
        elemFadeInOut($('div.slide-3-1'), scrolled, 2800, 2000)
        elemShowHide2($('div.slide-4-1'), scrolled, 1250, -100)
        elemShowHide2($('div.slide-4-2'), scrolled, 1150, -100)
    }
}

function elemFadeInOut(obj, scrolled, from, to){

    scrolled = parseInt(scrolled, 10)
    from = parseInt(from, 10)
    to = parseInt(to, 10)

    if( delta <= scrolled+from && delta > scrolled+to && obj.css('display') == 'none' && delta > 0 ){
        obj.fadeIn('slow')
    }else if( delta > scrolled+from && obj.css('display') == 'block' ){
        obj.fadeOut('slow')
    }else if( delta <= scrolled+to && obj.css('display') == 'block' ){
        obj.fadeOut('slow')
    }
}

function elemShowHide(obj, scrolled, from, to){

    scrolled = parseInt(scrolled, 10)
    from = parseInt(from, 10)
    to = parseInt(to, 10)

    if( delta <= scrolled+from && delta > scrolled+to && obj.css('display') == 'none' && delta > 0 ){
        obj.show()
    }else if( delta > scrolled+from && obj.css('display') == 'block' ){
        obj.hide()
    }else if( delta <= scrolled+to && obj.css('display') == 'block' ){
        obj.hide()
    }
}

function elemShowHide2(obj, scrolled, from, to){

    scrolled = parseInt(scrolled, 10)
    from = parseInt(from, 10)
    to = parseInt(to, 10)

    if( delta <= scrolled+from && delta > scrolled+to && obj.css('display') == 'none' && delta > 0 ){
        obj.show()
    }
}

function videoPlayChecker(){
    if ( !html5_support || video_paused )
        return false;

    video_paused = true
    video1.pause();
    video2.pause();
    video3.pause();
    video4.pause();
    video5.pause();
    video6.pause();
    video7.pause();
    you_tube_player.pauseVideo()
}

var startY = 0;

function ipadScrollInit(){

    window.addEventListener('orientationchange', function(){
        var scroll = 0
        $('html, body').animate({scrollTop: scroll }, {duration: 2800,easing: "easeInOutExpo"})
        curscroll = 0
        TweenLite.to($("div#content"), 2.8, {'-webkit-transform': 'translate(0px, -'+scroll+'px)', ease:'easeInOutExpo', onComplete:function(){
            curscroll = 0
        }} );
    });

    infoIpadInit()

    return false;

    $("div#content").css('-webkit-transform',
        'translate(0px, 0px)' );

    $(document).bind("dragstart", function() { return false; });

    $("div#content").on('touchstart', function(e) {
        e.preventDefault();
        startpos = e.originalEvent.touches[0].pageY;
        endscroll = -10;
    });

    $("div#content").on('touchmove', function(e) {
        e.preventDefault();
        endscroll = e.originalEvent.changedTouches[0].pageY;
        var diffscroll = endscroll-startpos;
        marginTop = curscroll + diffscroll;

        var tr = $("div#content").css('-webkit-transform');
        var values = tr.split('(')[1];
        values = values.split(')')[0];
        values = values.split(',');
        var y = values[5];


        if(marginTop > 0){
            marginTop = 0;
            curscroll = 0;
            $("div#content").css('-webkit-transform',
                'translate(0px, 0px)' );
            return false
        }

        if (window.matchMedia("(orientation: portrait)").matches) {
            var end_screen = -2900
        }

        if (window.matchMedia("(orientation: landscape)").matches) {
            var end_screen = -4080
        }

        if( marginTop < end_screen ){
            marginTop = end_screen;
            curscroll = end_screen;
            $("div#content").css('-webkit-transform',
                'translate(0px, '+end_screen+'px)' );
            return false
        }

//        $("div.console-ipad").html(marginTop)

        setIpadEffect(marginTop)

        $("div#content").css('-webkit-transform',
            'translate(0px, ' + marginTop + 'px)' );

    });

    $(document).on('touchend', function(e) {
        e.preventDefault();
        if(endscroll!=-10)var diffscroll = endscroll-startpos;
        else var diffscroll=0;

        curscroll = curscroll + diffscroll;
    });
}

function setIpadEffect(scrolled){
    if (window.matchMedia("(orientation: portrait)").matches) {

        if( scrolled < -1600 && $('div.slide-4-2').css('display') == 'none' ){
            $('div.slide-3-1').show()
        }

        if( scrolled < -2150 && $('div.slide-4-2').css('display') == 'none' ){
            $('div.slide-4-2').show()
        }

    }

    if (window.matchMedia("(orientation: landscape)").matches) {
        $("div.console-ipad").html('landscape')
    }
}

function infoIpadInit(){
    $("div.info-1").prepend('<div class="info-1-header"></div>')
    $("div.info-2").prepend('<div class="info-2-header"></div>')
    $("div.info-3").prepend('<div class="info-3-header"></div>')
    $("div.info-4").prepend('<div class="info-4-header"></div>')
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function mobileInit(){
    resizeMobile()
    $("img.logo").attr('src', '/assets/img/mobile/top_logo.png').width(90)
    window.addEventListener('orientationchange', function(){
        var t = (isAndroid)?300:0;
        setTimeout(function(){
            resizeMobile()
        }, t)
    });
}

function resizeMobile(){

    var w =  window.innerWidth,
        k = (window.orientation == 0)?10.15:9.53125,
        rc = (window.orientation == 0)?'mobile-landscape':'mobile-portrait',
        c = (window.orientation == 0)?'mobile-portrait':'mobile-landscape'

    $("div#mobile").height( Math.ceil(w*k)).removeClass(rc).addClass(c)

    $.each($("div.is_resize"), function(k, v){
        var m = (window.orientation == 0 || $(v).hasClass('not_resize'))?$(v).data('margin'):$(v).data('margin')-100
        m = (window.orientation != 0 && $(v).hasClass('not_resize'))?m-20:m
        $(v).css('top', Math.ceil(w*m/640)+'px')
    })

    $.each($("div.is_resize_header"), function(k, v){
        var cur_width = w/100*$(v).data('percent'),
            cur_height = Math.ceil(cur_width*$(v).data('height')/$(v).data('width'));
        $(v).height(cur_height)
    })

    $("img.mobile-youtube-play").width(Math.ceil(w*82/640))

    if(window.orientation == 0){
        $("div.info-text-landscape").hide()
        $("div.info-text-portrait").show()
    }else{
        $("div.info-text-landscape").show()
        $("div.info-text-portrait").hide()
    }


}