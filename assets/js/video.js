var video1,
    video2,
    video3,
    video4,
    video5,
    video6,
    video7;

var html5_support, you_tube_player, isAndroid;

var we_stopped_audio = false;
var ytplayer;

$(document).ready(function(){

    var ua = navigator.userAgent.toLowerCase();
    isAndroid = ua.indexOf("android") > -1

    if(isAndroid)
        $.ajax({
            url:"/assets/ipad/android.css",
            success:function(data){
                $("<style></style>").appendTo("head").html(data);
            }
        })

    var test_canvas = document.createElement("canvas")
    html5_support =(test_canvas.getContext)? true : false


    if( html5_support){
        var tag = document.createElement('script');
        tag.src = "http://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }else{
        youTubeInit()
    }

    video1 = videojs("video_1");
    video2 = videojs("video_2");
    video3 = videojs("video_3");
    video4 = videojs("video_4");
    video5 = videojs("video_5");
    video6 = videojs("video_6");
    video7 = videojs("video_7");

    video1.on("ended", function(){
        video2.play();
    });

    video4.on("ended", function(){
        video5.play();
    });

    video6.on("ended", function(){
        video7.play();
    });

    video1.on("play", function(){
        video_paused = false
    });
    video2.on("play", function(){
        video_paused = false
    });
    video3.on("play", function(){
        video_paused = false
    });
    video4.on("play", function(){
        video_paused = false
    })
    video5.on("play", function(){
        video_paused = false
    });
    video6.on("play", function(){
        video_paused = false
    });
    video7.on("play", function(){
        video_paused = false
    });
})

function youTubeInit(){
    var YouTubeURL = 'http://www.youtube.com/v/tBFo5GTv4H8?version=3&autoplay=0&rel=0&fs=0&theme=light&showinfo=0&modestbranding=1&hd=1&autohide=1&color=white&controls=1&enablejsapi=1';
    var params = { allowScriptAccess: "always"};
    var flashvars = {};
    var atts = { id: "myytplayer" };
    swfobject.embedSWF(YouTubeURL, "final_video", "695", "310", "9", null,
        flashvars, params, atts);
}

function onYouTubePlayerReady(playerId) {
    ytplayer = document.getElementById("myytplayer");
    ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
}

var done = false;
function onytplayerStateChange(newState){
    //���� ������, ���������� ��������
    if(newState == -1)
        ytplayer.setPlaybackQuality('hd720')

    //������������� �����, ���� ������� ����� ������, �� ��������� ��� � ������ ����� ��� ���������
    if(newState == 1){
        if($("div#audiojs_wrapper0").hasClass('playing')){
            $("p.play").trigger('click')
            we_stopped_audio = true
        }

        if (!done)
            checkInAnalytic('youtube_video_play')
        $("img.hidder-play").show()
        done = true
    }
    //����� �����, ���� ������� ����� �� ���������, �� �������� ���
    if(newState == 2){
        if(we_stopped_audio){
            $("p.play").trigger('click')
            we_stopped_audio = false
        }
        $("img.hidder-play").hide()
    }
}

function onYouTubeIframeAPIReady() {
    you_tube_player = new YT.Player('final_video', {
        height: '310',
        width: '695',
        playerVars: { 'autoplay': 0, 'theme':'light', 'autohide':1, 'color':'white', 'showinfo':0, 'modestbranding':1, 'hd':1, 'fs':0 },
        videoId: 'tBFo5GTv4H8',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

function onPlayerReady(){
    you_tube_player.setPlaybackQuality('hd720');
}

function onPlayerStateChange(event) {

    if (event.data == YT.PlayerState.PLAYING) {
        video_paused = false
        if($("div#audiojs_wrapper0").hasClass('playing')){
            $("p.play").trigger('click')
            we_stopped_audio = true
        }

        if (!done)
            checkInAnalytic('youtube_video_play')
        $("img.hidder-play").show()
        done = true
    }

    if(event.data == YT.PlayerState.PAUSED){
        if(we_stopped_audio){
            $("p.play").trigger('click')
            we_stopped_audio = false
        }
        $("img.hidder-play").hide()
    }

}