var timeoutObjectId, arrow_down_click = false;

$(document).ready(function(){

    $(".about_project").on('touchstart', function(){
        $("form#to_project").submit()
        checkInAnalytic('about_project')
    })

    $(".about_project").on('click', function(){
        $("form#to_project").submit()
        checkInAnalytic('about_project')
    })

    $("img.logo_goto").on('click', function(){
        $("form#to_lexus").submit()
        checkInAnalytic('desktop_to_lexus')
    })

    $("img.logo_goto, div.logo_goto").on('touchstart', function(){
        $("form#to_lexus").submit()
        if( jQuery.browser.mobile )
            checkInAnalytic('mobile_to_lexus')
        else
            checkInAnalytic('desktop_to_lexus')
    })


    $("div.arrow-down").on('click', function(){
        scrollTo($(this).data('goto'))
        arrow_down_click = true
    })

    $("div.arrow-up").on('click', function(){
        scrollTo($(this).data('goto'))
    })

    $(".mobile-youtube-play").on('touchstart', function(){
        checkInAnalytic('youtube_video_play')
    })

    if( !is_ipad )
        resizeObject()

})

function scrollToMobile(gt){
    var scrl = Math.ceil(parseInt($(Goto[gt]).css('top'), 10) - ($(window).height() - $(Goto[gt]).height())/3 )
    $('html, body').animate({scrollTop: scrl }, {duration: 2800,easing: "easeInOutExpo"})
}

function scrollToIpad(gt){
    if( Goto[gt] != 0 )
        var scroll = Math.ceil(parseInt($(Goto[gt]).css('top'), 10)-$(window).height()/3)
    else{
        var scroll = 0
        $('html, body').animate({scrollTop: scroll }, {duration: 2800,easing: "easeInOutExpo"})
    }

    curscroll = -(scroll)
    TweenLite.to($("div#content"), 2.8, {'-webkit-transform': 'translate(0px, -'+scroll+'px)', ease:'easeInOutExpo', onComplete:function(){
        curscroll = -(scroll)
    }} );
}

function scrollTo(gt){

    if( Goto[gt] != 0 ){

        var offset_top = $(Goto[gt]).data('top'),
            scroll_coff = $(Goto[gt]).data('coeff'),
            wh = $(window).height(),
            h = $(Goto[gt]).height();

        scrl = Math.ceil(offset_top/scroll_coff - ((wh-h)/2)/scroll_coff)

    }else{
        scrl = 0
    }

    if( Goto[gt] == 'div.info-3' || Goto[gt] == 'div.info-4' || Goto[gt] == 'div.info-5' ){
        scrl = offset_top/scroll_coff-(wh-h)/scroll_coff
    }

    if( is_ipad ){
        scrl = parseInt($(Goto[gt]).css('top'))-(wh-h)/2
        if( Goto[gt] == 'div.info-3' || Goto[gt] == 'div.info-4' || Goto[gt] == 'div.info-5' ){
            scrl = parseInt($(Goto[gt]).css('top'))-(wh-h)
        }
    }
    
    $('html, body').animate({scrollTop: scrl }, {duration: 2800,easing: "easeInOutExpo", complete:function(){
        video_paused = false
        arrow_down_click= false
        switch (gt){
            case 'step_1':
                if ( html5_support) video1.play();
                break;
            case 'step_2':
                if ( html5_support ) video3.play();
                break;
            case 'step_2_2':
                if ( html5_support ) video4.play();
                break;
            case 'step_4':
                setTimeout(function(){
//                    $("div.slide-3").hide()
                    $("div.slide-3-1").fadeIn(function(){
                        if ( html5_support ) video6.play();
                    })
                }, 500)
                break;
            case 'step_5':
                setTimeout(function(){
                    $("div.slide-4-1").show()
                }, 500)
                setTimeout(function(){
                    $("div.slide-4-2").show()
                }, 800)
                setTimeout(function(){
                    scrollTo('step_6')
                }, 1900)
                break;
            case 'step_6':
//                you_tube_player.playVideo()
                break;
        }
    }})
}

$(window).resize(function () {
    if( !is_ipad )
        resizeObject();
});

function resizeObject(){
    if( $(window).width() >= 800 ){
        clearTimeout(timeoutObjectId)
        timeoutObjectId = setTimeout(function(){
            var p = 100 - (($(window).width()-800)*100/1120)
            m = 910 - Math.ceil(700/100*p)
            m2 = 1529 - Math.ceil(900/100*p)
            m3 = Math.ceil(.1*p)
            $("div.men-1").animate({width: m }, {duration: 100,easing: "easeOutExpo"})
            $("div.men-2").animate({width: m2 }, {duration: 100,easing: "easeOutExpo"})
            $("div.leaves-top").css('width', (90+m3)+'%')
        }, 100)
    }else{
        $("div.men-1").animate({width: 210 }, {duration: 100,easing: "easeOutExpo"})
        $("div.men-2").animate({width: 629 }, {duration: 100,easing: "easeOutExpo"})
    }

//    if( $(window).width() > 1800 && $("div.leaves").data('size') == 'original' ){
//        $("div.leaves").css('backgroundImage', 'url(/assets/img/leaves_big.png)').data('size', 'big')
//    }else if( $("div.leaves").data('size') == 'big' ){
//        $("div.leaves").css('backgroundImage', 'url(/assets/img/leaves.png)').data('size', 'original')
//    }
}