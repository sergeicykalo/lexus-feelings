var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-41016585-1']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

function checkInAnalytic(type){
    switch (type){
        case 'desktop':
            _gaq.push(['_trackPageview', '#desktop']);
        break;
        case 'mobile':
            _gaq.push(['_trackPageview', '#mobile']);
            break;
        case 'desktop_to_lexus':
            _gaq.push(['_trackPageview', '#desktop_to_lexus']);
            break;
        case 'mobile_to_lexus':
            _gaq.push(['_trackPageview', '#mobile_to_lexus']);
            break;
        case 'about_project':
            _gaq.push(['_trackPageview', '#about_project']);
            break;
        case 'youtube_video_play':
            _gaq.push(['_trackPageview', '#youtube_video_play']);
            break;
        case 'qr_video':
            _gaq.push(['_trackPageview', '#qr_video']);
            break;
    }
}